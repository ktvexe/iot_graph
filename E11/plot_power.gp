if (!exists("filename")) filename='0826_power'
if (!exists("titlename")) titlename='0826 power event-sequence'
if (!exists("outputfile")) outputfile='0826_power_event-sequence.png'
if (!exists("begintime")) begintime='06:30:00'
if (!exists("endtime")) endtime='10:30:00'

reset
set title titlename
set term png enhanced font 'Verdana,10'
set output outputfile
#set ytics 1
set xdata time
set timefmt "%H:%M:%S"
set format x "%H:%M:%S"
set xrange [begintime:endtime]
set style increment user
set style line 1 lc rgb 'red' 
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'green'
set style line 4 lc rgb 'magenta'
set style line 5 lc rgb 'yellow'
set xtics rotate by -45
set grid
set style data points
plot [:][:]filename using 2:4 with line lw 2 
