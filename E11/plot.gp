if (!exists("filename")) filename='0428_r2work'
if (!exists("titlename")) titlename='0428 R2_work event-sequence'
if (!exists("outputfile")) outputfile='0428_work_event-sequence.png'
if (!exists("begintime")) begintime='23:14:33'
if (!exists("endtime")) endtime='23:35:22'

reset
set title titlename
set term png enhanced font 'Verdana,10'
set output outputfile
set ytics 1
set xdata time
set timefmt "%H:%M:%S"
set format x "%H:%M:%S"
set xrange [begintime:endtime]
set style increment user
set style line 1 lc rgb 'red' 
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'green'
set style line 4 lc rgb 'magenta'
set style line 5 lc rgb 'yellow'
set xtics rotate by -45
set grid
set style data points
plot [:][:] filename using 2:3:4 linecolor variable pt 1 ps 1 t ''
