if (!exists("filename")) filename='0217'
if (!exists("titlename")) titlename='0217 event-sequence'
if (!exists("outputfile")) outputfile='event-sequence.png'
if (!exists("begintime")) begintime='18:00:00'
if (!exists("endtime")) endtime='20:00:00'
reset
set title 'titlename'
set term png enhanced font 'Verdana,10'
set output 'outputfile'
set ytics 1
set xdata time
set timefmt "%H:%M:%S"
set format x "%H:%M:%S"
set xtics rotate by -45 
set xrange [begintime:endtime]
set style increment user
set style line 1 lc rgb 'red' 
set style line 2 lc rgb 'blue'
set style line 3 lc rgb 'green'
set style line 4 lc rgb 'magenta'
set style line 5 lc rgb 'yellow'

set style data points
plot [:][:] filename using 2:3:4 linecolor variable pt 1 ps 1 t ''
# plot [:][0:4]filename using 2:4 with points title ''
