# preprocess data to separate events by time
# Usage e.g. python preprocess.py data "Sleeping begin" "Sleeping end"

import sys
def preproc(name,begin,end):
    with open(name,"r+") as file_a:
        i=0
        while True:
            file_string=file_a.readline()
            if not file_string:
                break
            position_begin=file_string.find(begin)
            if position_begin != -1:
                with open(begin,"a") as file_write:
                    file_write.write(begin+str(i)+":\n")
                    i=i+1
                    file_write.write(file_string)
                    while True:
                        file_string=file_a.readline()
                        position_end=file_string.find(end)
                        if position_end != -1:
                            file_write.write(file_string)
                            file_write.close()
                            break
                        file_write.write(file_string)


def main():
    if len(sys.argv) < 4:
        print "Usage:",sys.argv[0],"<filename> <beginString> <endString>"
        sys,exit(1)
    fileName = sys.argv[1]
    beginString = sys.argv[2]
    endString = sys.argv[3]
    preproc(fileName,beginString,endString)


if __name__ == "__main__":
    main()


