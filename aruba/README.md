## How to plot sequence-dot graph

1. Install gnuplot `sudo apt-get install gnuplot`.
2. Usage: `gnuplot -e "filename='your_input_file'" plot.gp`.
    - e.g. `gnuplot -e "filename='0217'" plot.gp`.
3. Change the corresponding parameter in the same way if you wanna change title, output name, and so on. For more information, please take a look **plot.gp**.

## How to preprocess data

* Usage: `python preprocess.py <filename> <beginString> <endString>`
    - e.g. `python preprocess.py data "Sleeping begin" "Sleeping end"`

## Introduction

This dataset contains sensor data that was collected in the home of
a volunteer adult.  The resident in the home was a woman.  The woman's
children and grandchildren visited on a regular basis.

The following activities are annotated within the dataset. The number in
parentheses is the number of times the activity appears in the data.

* Meal_Preparation (1606)
* Relax (2910)
* Eating (257)
* Work (171)
* Sleeping (401)
* Wash_Dishes (65)
* Bed_to_Toilet (157)
* Enter_Home (431)
* Leave_Home (431)
* Housekeeping (33)
* Resperate (6)

The sensor events are generated from motion sensors (these sensor IDs begin
with "M"), door closure sensors (these sensor IDs begin with "D"), and
temperature sensors (these sensor IDs begin with "T").
The layout of the sensors in the home is shown in the file aruba.jpg.

You may download and analyze this data free of charge.  Please do not
distribute the data without explicit permission from the CASAS research group.
Please reference the CASAS project in any publications resulting from using
this dataset.

All use of the data must cite the WSU CASAS smart home project.

D. Cook. Learning setting-generalized activity mdoels for smart spaces.
IEEE Intelligent Systems, 2011.
